// Checks current number of columns
function currentSize() {
  if ($(".size-check").css("float") == "left" ){ 
    return 2
  } else {
    return 4
  }
}

$(document).ready(function() {
    // Sets number of columns on load
    number_of_cols = currentSize()
    // Sorts members/bios on load
    teamSort();



    // Opens Bio on click
    $('body').on('click', '.member', function() {
      
      memberIndex = $(this).index('.member')
      bioIndex = Math.floor(memberIndex / currentSize())
      openBio = Math.floor($('.open').index('.member') / currentSize())
      
      $('.show').removeClass('show')
      $('.about-prev').hide()
      $('.bio-text').eq(memberIndex).children().children('p').first().addClass('show')
      /* Displays nav if p count > 1 */
      var text = $('.bio').eq(memberIndex).children().children()
      var ps = text.children('p')
      var count = ps.length

      if (count > 1) {
        $('.about-next').eq(memberIndex).show()
      }

      if (bioIndex !== openBio) {
        $('.bios').slideUp()
      }

      if ( $(this).hasClass('open') ) {
        $('.bio').fadeOut(100)
        $('.bios').slideUp()  
        $('.member').removeClass('open')
      } else {
        $('.bio').fadeOut(100)
        $('.bio').eq(memberIndex).fadeIn(100)
        $('.bios').eq(bioIndex).slideDown()
        $('.member').removeClass('open')
        $(this).addClass('open')

      }

    })
    $('.about-next, .about-prev').css('cursor','pointer')
    // Next Button
    $('body').on('click touchstart', '.about-next', function(event) {
          event.preventDefault()

      var div = $(this).parent().parent().children('p')
      var showIndex = div.siblings('.show').index()
      var nextIndex = showIndex
      
      div.removeClass('show')
      div.eq(nextIndex).addClass('show')
      $('.about-prev').show()

      if (!$('.show').next().is('p')) {
        $(this).hide()
      }
      return false;
    })

    // previous button
    $('body').on('click touchstart', '.about-prev', function(event) {
          event.preventDefault()

      var div = $(this).parent().parent().children('p')
      var showIndex = div.siblings('.show').index()
      var nextIndex = showIndex - 2
      
      div.removeClass('show')
      div.eq(nextIndex).addClass('show')
      $('.about-next').show()

      if (!$('.show').prev().is('p')) {
        $(this).hide()
      }
      return false;
    })

});

// Checks for breakpoint on resize
$(window).resize(function(){
  // Resorts if breakpoint is hit
  if (currentSize() != number_of_cols) {
    number_of_cols = currentSize()
    teamSort();
  }
});

// Dom Sort function
function teamSort() {
  member_queue = [];
  bio_queue = [];

  $(".team .member").each(function(index) {
    member_queue.push(this);
    $(this).remove();
  });

  $(".team .bio").each(function(index) {
    bio_queue.push(this);
    $(this).remove();
  });

  $(".team .bios").remove();

  while(member_queue.length > 0) {
    for(var i = 0; i < number_of_cols; i++) {
      $(".team").append(member_queue.shift());
    }

    $(".team").append($("<div>").addClass("bios"));

    for(var i = 0; i < number_of_cols; i++) {
      $(".team .bios").last().append(bio_queue.shift());
    }
  }
}
