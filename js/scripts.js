$('.menu-icon').mouseenter(function(){
  if ($(".size-check").css("float") == "none" ){
    $('.overlay').css('z-index', '9').addClass('open')
    $('.master-nav').addClass('nav-open')
  }
})

$('.master-nav').mouseleave(function(){
  if ($(".size-check").css("float") == "none" ){
    $('.overlay').removeClass('open')
    $('.master-nav').removeClass('nav-open')
    setTimeout(function() {
          $('.overlay').css('z-index', '-1')
    }, 200);
  }
})

$('.menu-icon').click(function(){
  if ($(".size-check").css("float") == "left" ){
    $('.overlay').css('z-index', '9').addClass('open')
    $('.master-nav').addClass('nav-open')
  }
})

$('.overlay').click(function(){
  if ($(".size-check").css("float") == "left" ){
    $('.master-nav').removeClass('nav-open')
    $('.overlay').removeClass('open')
    setTimeout(function() {
          $('.overlay').css('z-index', '-1')
    }, 200);
  }
})

$('.thumbs li').hover(function(e){
  e.preventDefault()
  
})

$('.thumbs li').click(function(){
  $('.open').removeClass('open')
  $(this).children('figure').addClass('open');
});


$(document).ready(function(){
  $('.project-slider-video').bxSlider({
    video: true,
    useCSS: false,
    auto: false
  });

  $('.project-slider').bxSlider({
    video: false,
    useCSS: false,
    auto: true,
    pause: 4000
  });

})